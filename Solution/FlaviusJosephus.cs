﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution
{
    public sealed class FlaviusJosephus
    {
        private List<Human> humans;
        private int count;
        private int index;

        public IEnumerable<string> StartCount(int numberOfHumans, int number)
        {
            if (numberOfHumans <= 0)
            {
                throw new ArgumentException("The number of humans must be greater or equal to one.",  nameof(numberOfHumans));
            }
            
            if (number <= 0)
            {
                throw new ArgumentException("The number must be greater or equal to one.", nameof(number));
            }

            this.humans = new List<Human>();

            for (int i = 1; i<= numberOfHumans; i++)
            {
                this.humans.Add(new Human(i));
            }

            this.count = 1;
            this.index = 0;

            return ThrowingOutHumans(number);
        }

        private IEnumerable<string> ThrowingOutHumans(int number)
        {
            while (this.humans.Count != 1)
            {
                if (this.index == humans.Count)
                {
                    this.index = 0;
                }

                if (this.count == number)
                {
                    yield return $"{this.humans[index]} dropped out";
                    this.humans.RemoveAt(index);
                    index--;
                    count = 0;
                    continue;
                }

                index++;
                count++;
            }

        }
    }
}
