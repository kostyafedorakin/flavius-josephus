﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution
{
    internal struct Human
    {
        public Human(int number)
        {
            this.Number = number;
        }

        int Number { get; }

        public override string ToString() => $"The person with the number {this.Number}.";
    }
}
