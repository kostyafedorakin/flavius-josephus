﻿using NUnit.Framework;
using Solution;
using System;
using System.Collections.Generic;

namespace Test
{
    [TestFixture]
    public class FlaviusJosephusTest
    {
        private static IEnumerable<TestCaseData> DataCases
        {
            get
            {
                yield return new TestCaseData(2, 2, new string[] { 
                    "The person with the number 2. dropped out" 
                });
                yield return new TestCaseData(3, 2, new string[] {
                    "The person with the number 2. dropped out",
                    "The person with the number 1. dropped out",
                });
                yield return new TestCaseData(3, 1, new string[] {
                    "The person with the number 1. dropped out",
                    "The person with the number 2. dropped out",
                });
                yield return new TestCaseData(3, 3, new string[] {
                    "The person with the number 3. dropped out",
                    "The person with the number 1. dropped out",
                });
            }
        }
        private FlaviusJosephus solver = new FlaviusJosephus();

        [TestCaseSource(nameof(DataCases))]
        public void SimleTest(int numberOfHumans, int count, string[] expected)
        {
            CollectionAssert.AreEqual(solver.StartCount(numberOfHumans, count), expected);
        }

        [TestCase(0)]
        [TestCase(-1)]
        [TestCase(-23423)]
        public void ThrownExceptionWhenNumberOfHumansLessOne(int number)
        {
            Assert.Throws<ArgumentException>(
                () => this.solver.StartCount(10, number)
                );
        }

        [TestCase(0)]
        [TestCase(-1)]
        [TestCase(-23423)]
        public void ThrownExceptionWhenCountLessOne(int number)
        {
            Assert.Throws<ArgumentException>(
                () => this.solver.StartCount(number, 10)
                );
        }
    }
}
